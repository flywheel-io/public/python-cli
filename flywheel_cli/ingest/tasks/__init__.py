"""This module holds the different type of task classes."""

from .factory import create_task  # noqa: F401
