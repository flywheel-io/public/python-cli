"""Flywheel CLI package."""

import os
from importlib import metadata

__version__ = metadata.version(__name__)

os.environ["SQLALCHEMY_SILENCE_UBER_WARNING"] = "1"
