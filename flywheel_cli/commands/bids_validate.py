"""Bids validator subcommand."""

BIDS_IMAGE = "bids/validator:v1.14.12"


def add_command(subparsers, parents):
    """Add bids validate command."""
    parser = subparsers.add_parser(
        "validate",
        parents=parents,
        help="Validate a BIDS folder using the official BIDS validator",
    )
    parser.add_argument(
        "folder", metavar="[dest folder]", help="The path to the bids folder"
    )
    parser.set_defaults(func=validate_bids)
    parser.set_defaults(parser=parser)

    return parser


def validate_bids(args):
    """Validate bids."""
    import subprocess
    from pathlib import Path

    volume = f"-v={Path(args.folder).absolute()}:/data:ro"
    cmd = ["docker", "run", "-it", "--rm", volume, BIDS_IMAGE, "/data"]
    subprocess.run(cmd, check=True)
