package delegate

import (
	"os"
	"path"
)

var CachePath string

func init() {
	FlywheelUserHome, set := os.LookupEnv("FLYWHEEL_USER_HOME")
	if !set {
		FlywheelUserHome = "~"
	}
	CachePath = path.Join(FlywheelUserHome, ".cache/flywheel")
}
