package delegate

import (
	"archive/tar"
	"bytes"
	"compress/gzip"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"os/signal"
	"path/filepath"
	"runtime"
	"syscall"

	homedir "github.com/mitchellh/go-homedir"

	"flywheel.io/fw/python/pkgdata"
	. "flywheel.io/fw/util"
)

type PkgVersionInfo struct {
	PythonVersion string `json:"python_version",omitempty`
	PyVer         string `json:"py_ver",omitempty`
	CliVersion    string `json:"cli_version",omitempty`
	Platform      string `json:"platform",omitempty`
	BuildTime     string `json:"build_time",omitempty`
}

type PythonPathInfo struct {
	PythonBinDir    string
	SitePackagePath string
}

var PythonCliCommand = []string{"-I", "-m", "flywheel_cli.main"}

const LibcExec = "libc-exe"
const SitePackagesName = "site-packages.zip"

// Exits if the command is delegated
func DelegateCommandToPython() {
	// Expand cacheDir
	cacheDir, err := homedir.Expand(CachePath)
	if err != nil {
		fmt.Print("Could not locate cache dir", err)
		Fatal(1)
	}

	// Extract python and site-packages
	pythonPathInfo, err := PopulateCache(cacheDir)
	if err != nil {
		fmt.Print("Could not delegate command", err)
		Fatal(1)
	}

	// Build the command string for the current platform
	var prog []string
	if runtime.GOOS == "windows" {
		prog = []string{filepath.Join(pythonPathInfo.PythonBinDir, "python.exe")}
	} else {
		prog = []string{filepath.Join(pythonPathInfo.PythonBinDir, "bin/python3")}
	}

	// Passthrough all args beyond program
	// NOTE: The args passed into this function are not always complete,
	// especially in the case of help invocation. Hence the direct use of
	// os.Args instead.
	if len(os.Args) == 2 && os.Args[1] == "python" {
		prog = append(prog, "-I")
		prog = append(prog, os.Args[2:]...)
	} else {
		prog = append(prog, PythonCliCommand...)
		prog = append(prog, os.Args[1:]...)
	}

	execCommand := exec.Command
	// Launch the python command with piped stdin/stdout
	cmd := execCommand(prog[0], prog[1:]...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Env = append(os.Environ(),
		fmt.Sprintf("PYTHONHOME=%s", pythonPathInfo.PythonBinDir),
		fmt.Sprintf("PYTHONPATH=%s", pythonPathInfo.SitePackagePath),
	)

	// Setup our Ctrl+C handler
	SetupCloseHandler()

	err = cmd.Run()
	if err != nil {
		fmt.Println(err)
		Fatal(1)
	}
}

// SetupCloseHandler creates a 'listener' on a new goroutine which will notify the
// program if it receives an interrupt from the OS.
// We need this because the ingest handles the first SIGTERM, but does not
// force exit all workers - however the 2nd SIGTERM would not reach python if
// we exit the GO program - this way the python might not exit
func SetupCloseHandler() {
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		fmt.Println("\nCtrl+C pressed, terminating...")
	}()
}

// Extract the python interpreter and site-packages.zip to the cache directory
func PopulateCache(cacheDir string) (*PythonPathInfo, error) {
	// Get version info
	data, err := pkgdata.Asset("version.json")
	if err != nil {
		return nil, err
	}

	versionInfo := PkgVersionInfo{}
	err = json.Unmarshal(data, &versionInfo)
	if err != nil {
		return nil, err
	}

	// Resolve paths relative to cache dir
	pythonDistName := fmt.Sprintf("python-%s-%s", versionInfo.PythonVersion, versionInfo.Platform)
	cliDistName := fmt.Sprintf("flywheel-cli-%s-%s", versionInfo.CliVersion, versionInfo.Platform)
	pythonDir := filepath.Join(cacheDir, pythonDistName)
	var pythonLibDir string
	if runtime.GOOS == "windows" {
		pythonLibDir = filepath.Join(pythonDir, "Lib")
	} else {
		pythonLibDir = filepath.Join(pythonDir, "lib", fmt.Sprintf("python%s", versionInfo.PyVer))
	}
	sitePkgPath := filepath.Join(pythonLibDir, "site-packages")

	// Extract package
	buildTimePath := filepath.Join(cacheDir, ".site-packages.buildtime")
	buildTime, err := ioutil.ReadFile(buildTimePath)
	if err != nil || string(buildTime) != versionInfo.BuildTime {
		// Delete previous version and extract asset
		fmt.Println("Extracting assets...")
		os.RemoveAll(pythonDir)
		// Extract python standalone binary
		err = ExtractAssetTar(pythonDistName+".tar.gz", pythonDir)
		if err != nil {
			return nil, err
		}
		// Extract cli dist
		err = ExtractAssetTar(cliDistName+".tar.gz", pythonDir)
		if err != nil {
			return nil, err
		}
		// Write the build time to .site-packages.buildtime
		err = ioutil.WriteFile(buildTimePath, []byte(versionInfo.BuildTime), os.FileMode(0644))
		if err != nil {
			return nil, err
		}
	}

	return &PythonPathInfo{pythonDir, sitePkgPath}, nil
}

func ExtractAssetTar(assetName, dst string) error {
	data, err := pkgdata.Asset(assetName)
	if err != nil {
		return err
	}

	// Extract asset
	byteReader := bytes.NewReader(data)
	uncompressedStream, err := gzip.NewReader(byteReader)
    if err != nil {
        return err
    }
	tarReader := tar.NewReader(uncompressedStream)

	for {
		header, err := tarReader.Next()

		switch {
		case err == io.EOF:
			return nil
		case err != nil:
			return err
		case header == nil:
			continue
		}

		target := filepath.Join(dst, header.Name)

		switch header.Typeflag {
		case tar.TypeDir:
			if _, err := os.Stat(target); err != nil {
				if err := os.MkdirAll(target, 0700); err != nil {
					return err
				}
			}
		case tar.TypeSymlink:
			link := filepath.Join(dst, header.Linkname)
			rel, err := filepath.Rel(dst, link)
			if err != nil {
				return err
			}

			if err := os.Symlink(rel, target); err != nil {
				return err
			}
		case tar.TypeReg:
			f, err := os.OpenFile(target, os.O_CREATE|os.O_RDWR, os.FileMode(header.Mode))
			if err != nil {
				return err
			}
			if _, err := io.Copy(f, tarReader); err != nil {
				return err
			}
			f.Close()
		}
	}

	return nil
}
