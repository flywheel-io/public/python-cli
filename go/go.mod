module flywheel.io/fw

go 1.18

require (
	github.com/fatih/color v1.7.1-0.20181010231311-3f9d52f7176a
	github.com/mitchellh/go-homedir v1.1.0
)

require (
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	golang.org/x/sys v0.6.0 // indirect
)
