package main

import (
	"os"

	"flywheel.io/fw/delegate"
	. "flywheel.io/fw/util"
)

var Version = "dev"
var BuildHash = "dev"
var BuildDate = "dev"

func InvokeCommand(args []string) int {
	VersionString = "Flywheel CLI " + Version + " build " + BuildHash + " on " + BuildDate
	defer GracefulRecover()
	delegate.DelegateCommandToPython()
	return 0
}

func main() {
	os.Exit(InvokeCommand(os.Args))
}
