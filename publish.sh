#!/usr/bin/env bash
set -Eeuo pipefail
PS4='+ ${BASH_SOURCE#$PWD\/}:${LINENO} ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
cg=$'\033[1;32m'
c0=$'\033[0m'
test -z "${DEBUG:-}" || set -x

main() {
    export FORCE_COLOR=1
    export GOCACHE=$PWD/.cache/go
    export PIP_CACHE_DIR=$PWD/.cache/pip
    export POETRY_CACHE_DIR=$PWD/.cache/poetry
    make
    cd dist
    for file in $(find . -type f -name '*.zip' | sed -E 's|^./||' | sort); do
        file_sha=$file.sha256
        prefix=${CI_COMMIT_REF_NAME/#(master|main)/latest}
        xh -IF "$FLYWHEEL_DIST_URL/$prefix/$file_sha" >"/tmp/$file_sha" 2>/dev/null || :
        if diff -q "$file_sha" "/tmp/$file_sha" >/dev/null; then
            log "Skipping $file (SHA matches)"
        else
            test -s "/tmp/$file_sha" && action=Overwriting || action=Uploading
            log "$action $FLYWHEEL_DIST_URL/$prefix/$file"
            gs cp "$file*" "$FLYWHEEL_DIST/$prefix"
            gs acl ch -u AllUsers:R "$FLYWHEEL_DIST/$prefix/$file*"
        fi
    done
}

log() {( set +x; printf >&2 "${cg}INFO${c0} %b\n" "$*"; )}
gs() { gsutil \
    -o "Credentials:gs_service_key_file=$GOOGLE_APPLICATION_CREDENTIALS" \
    -h "Cache-Control:no-cache,max-age=0" -m "$@"
}

main "$@"
