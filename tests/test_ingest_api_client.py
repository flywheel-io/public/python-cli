import datetime
import importlib
import io
import json
import os
from unittest import TestCase
from unittest.mock import call
from uuid import UUID, uuid4

import pytest
from fw_http_client.client import Response
from fw_http_client.errors import ServerError
from requests.exceptions import ConnectionError as ReqConnError

from flywheel_cli import util
from flywheel_cli.ingest import config
from flywheel_cli.ingest import schemas as T
from flywheel_cli.ingest.client import api as api_client
from flywheel_cli.models import FWAuth

pytest_plugins = "fw_http_testserver"

_API_KEY = "apikey"


def test_instantiate_client(mock_get_api_key, client, core):
    assert isinstance(client, api_client.APIClient)
    assert client.url == core.url
    assert client._ingest_id is None
    mock_get_api_key.assert_called_once()
    assert client.session.headers["Authorization"] == mock_get_api_key.return_value


def test_create_from_url(mock_get_api_key, core):
    client = api_client.APIClient.from_url(core.url)

    assert isinstance(client, api_client.APIClient)
    assert client.url == core.url
    mock_get_api_key.assert_called_once()
    assert client.session.headers["Authorization"] == mock_get_api_key.return_value
    assert client._ingest_id is None


def test_create_from_url_with_uuid(mock_get_api_key, core):
    uuid = uuid4()
    client = api_client.APIClient.from_url(core.url, uuid)

    assert isinstance(client, api_client.APIClient)
    assert client.url == core.url
    mock_get_api_key.assert_called_once()
    assert client.session.headers["Authorization"] == mock_get_api_key.return_value
    assert client._ingest_id == uuid


def test_call_api(client, core):
    response = {"foo": "sample_response"}
    assert_core_response(core, client, "GET", "/foo", response)


def test_call_api_retry_statuses(mocker, client, core):
    time = mocker.patch("urllib3.util.retry.time")
    core.add_response("/status/502", {}, status=502)
    with pytest.raises(ServerError):
        client.call_api("GET", "/status/502")
    # sleeptime is 0 on 1st retry, then backoff * 2^(retry-1)
    assert time.sleep.mock_calls == [call(1.0), call(2.0), call(4.0), call(8.0)]


def test_call_api_no_retry_statuses(mocker, client, core):
    time = mocker.patch("urllib3.util.retry.time")
    core.add_response("/status/502", method="POST", status=502)
    with pytest.raises(ServerError):
        client.call_api("POST", "/status/502")
    assert time.sleep.mock_calls == []


def test_call_api_error_is_retried(mocker, client):
    sleep_mock = mocker.patch("urllib3.util.retry.Retry.sleep")
    with pytest.raises(ReqConnError):
        client.session.get("https://flywheel.test")
    assert sleep_mock.call_count == client.session.config.retry_total


def test_create_ingest(client, core):
    response = {
        "id": "18f61a79-208f-4201-91cc-5adff8d600f0",
        "label": "label",
        "fw_host": "fw_host",
        "fw_user": "fw_user",
        "config": {"src_fs": "/tmp"},
        "strategy_config": {
            "dicom": "dicom",
            "root_dirs": 0,
            "no_subjects": False,
            "no_sessions": False,
            "strategy_name": "folder",
        },
        "status": "created",
        "timestamp": 0,
        "history": [("created", 0)],
        "created": "2020-01-01 10:00:00",
    }
    core.add_response("/ingests", response)
    core.add_response("/ingests", response, method="POST")  # handle POST request
    cfg = config.IngestConfig(src_fs="/tmp")

    fw_auth = FWAuth(
        api_key="api_key",
        host="flywheel.test",
        user_id="test@flywheel.test",
        is_admin=True,
        is_device=False,
    )
    strg = config.FolderConfig()
    ingest = client.create_ingest(cfg, strg, fw_auth)
    payload = {
        "config": cfg.model_dump(exclude_none=True),
        "strategy_config": strg.model_dump(exclude_none=True),
    }
    assert_core_response(core, client, "POST", "/ingests", payload)

    # check bind
    assert str(client.ingest_id) == response["id"]
    assert str(ingest.id) == response["id"]

    assert_ingest(ingest, response)


def test_list_ingests(client, core):
    response = [
        {
            "id": "18f61a79-208f-4201-91cc-5adff8d600f0",
            "label": "label",
            "fw_host": "fw_host",
            "fw_user": "fw_user",
            "config": {"src_fs": "/tmp"},
            "strategy_config": {
                "dicom": "dicom",
                "root_dirs": 0,
                "no_subjects": False,
                "no_sessions": False,
                "strategy_name": "folder",
            },
            "status": "created",
            "timestamp": 0,
            "history": [("created", 0)],
            "created": "2020-01-01 10:00:00",
        },
        {
            "id": "11111111-2222-3333-4444-555555555555",
            "label": "label",
            "src_fs": "src_fw",
            "fw_host": "fw_host",
            "fw_user": "fw_user",
            "config": {"src_fs": "/tmp"},
            "strategy_config": {
                "dicom": "dicom",
                "root_dirs": 0,
                "no_subjects": False,
                "no_sessions": False,
                "strategy_name": "folder",
            },
            "status": "created",
            "timestamp": 0,
            "history": [("created", 0)],
            "created": "2020-01-01 10:00:00",
        },
    ]
    core.add_response("/ingests", response)
    ingests = list(client.list_ingests())
    assert_core_response(core, client, "GET", "/ingests", response)

    assert len(ingests) == 2

    for i in range(len(ingests)):
        assert_ingest(ingests[i], response[i])


def test_ingest(client, core):
    response = {
        "id": "18f61a79-208f-4201-91cc-5adff8d600f0",
        "label": "label",
        "fw_host": "fw_host",
        "fw_user": "fw_user",
        "config": {"src_fs": "/tmp"},
        "strategy_config": {
            "dicom": "dicom",
            "root_dirs": 0,
            "no_subjects": False,
            "no_sessions": False,
            "strategy_name": "folder",
        },
        "status": "created",
        "timestamp": 0,
        "history": [("created", 0)],
        "created": "2020-01-01 10:00:00",
    }

    uuid = response["id"]
    client.bind(uuid)
    core.add_response(f"/ingests/{uuid}", response)

    ingest = client.ingest
    assert_core_response(core, client, "GET", f"/ingests/{uuid}", response)

    assert str(ingest.id) == uuid


def test_load_subject_csv(client, core):
    response = {"foo": "bar"}

    uuid = "18f61a79-208f-4201-91cc-5adff8d600f0"
    client.bind(uuid)
    core.add_response(f"/ingests/{uuid}/subjects", response, method="POST")

    f = io.BytesIO(b"file_content")
    client.load_subject_csv(f)
    assert_core_response(core, client, "GET", f"/ingests/{uuid}/subjects", response)


def test_start(client, core):
    response = {
        "id": "18f61a79-208f-4201-91cc-5adff8d600f0",
        "label": "label",
        "fw_host": "fw_host",
        "fw_user": "fw_user",
        "config": {"src_fs": "/tmp"},
        "strategy_config": {
            "dicom": "dicom",
            "root_dirs": 0,
            "no_subjects": False,
            "no_sessions": False,
            "strategy_name": "folder",
        },
        "status": "created",
        "timestamp": 0,
        "history": [("created", 0)],
        "created": "2020-01-01 10:00:00",
    }

    uuid = response["id"]
    client.bind(uuid)
    core.add_response(f"/ingests/{uuid}/start", response, method="POST")
    ingest = client.start()

    assert_ingest(ingest, response)
    assert_core_response(core, client, "GET", f"/ingests/{uuid}", response)


def test_review(client, core):
    response = {
        "id": "18f61a79-208f-4201-91cc-5adff8d600f0",
        "label": "label",
        "fw_host": "fw_host",
        "fw_user": "fw_user",
        "config": {"src_fs": "/tmp"},
        "strategy_config": {
            "dicom": "dicom",
            "root_dirs": 0,
            "no_subjects": False,
            "no_sessions": False,
            "strategy_name": "folder",
        },
        "status": "in_review",
        "timestamp": 0,
        "history": [("in_review", 0)],
        "created": "2020-01-01 10:00:00",
    }

    uuid = response["id"]
    client.bind(uuid)

    changes = {"change": "change"}
    core.add_response(f"/ingests/{uuid}/review", response, method="POST")
    ingest = client.review(changes)

    assert_ingest(ingest, response)
    assert_core_response(core, client, "POST", f"/ingests/{uuid}/review", changes)


def test_abort(client, core):
    response = {
        "id": "18f61a79-208f-4201-91cc-5adff8d600f0",
        "label": "label",
        "fw_host": "fw_host",
        "fw_user": "fw_user",
        "config": {"src_fs": "/tmp"},
        "strategy_config": {
            "dicom": "dicom",
            "root_dirs": 0,
            "no_subjects": False,
            "no_sessions": False,
            "strategy_name": "folder",
        },
        "status": "aborted",
        "timestamp": 0,
        "history": [("aborted", 0)],
        "created": "2020-01-01 10:00:00",
    }

    uuid = response["id"]
    client.bind(uuid)
    core.add_response(f"/ingests/{uuid}", response)
    core.add_response(f"/ingests/{uuid}/abort", response, method="POST")

    ingest = client.abort()

    assert_ingest(ingest, response)
    assert_core_response(core, client, "POST", f"/ingests/{uuid}/abort", response)


def test_progress(client, core):
    response = {
        "scans": {
            "scanned": 1,
            "pending": 2,
        },
        "items": {
            "finished": 8,
            "total": 9,
        },
        "files": {
            "scanned": 1,
            "failed": 4,
            "canceled": 5,
            "total": 9,
        },
        "bytes": {
            "scanned": 1,
            "pending": 2,
            "completed": 6,
            "skipped": 7,
        },
    }

    uuid = "18f61a79-208f-4201-91cc-5adff8d600f0"
    client.bind(uuid)
    core.add_response(f"/ingests/{uuid}/progress", response)

    progress = client.progress
    assert_core_response(core, client, "GET", f"/ingests/{uuid}/progress", response)

    assert isinstance(progress, T.Progress)
    for key1 in ["scans", "items", "files", "bytes"]:
        v1 = getattr(progress, key1)
        m1 = response[key1]
        for key2 in [
            "scanned",
            "pending",
            "running",
            "failed",
            "canceled",
            "completed",
            "skipped",
            "finished",
            "total",
        ]:
            value = getattr(v1, key2)
            if key2 in m1:
                assert value == m1[key2]
            else:
                assert value == 0


def test_summary(client, core):
    response = {
        "groups": 1,
        "projects": 2,
        "subjects": 3,
        "sessions": 4,
        "acquisitions": 5,
        "files": 6,
        "packfiles": 7,
    }

    uuid = "18f61a79-208f-4201-91cc-5adff8d600f0"
    client.bind(uuid)
    core.add_response(f"/ingests/{uuid}/summary", response)

    summary = client.summary
    assert_core_response(core, client, "GET", f"/ingests/{uuid}/summary", response)

    assert isinstance(summary, T.Summary)
    for key in [
        "groups",
        "projects",
        "subjects",
        "sessions",
        "acquisitions",
        "files",
        "packfiles",
    ]:
        assert getattr(summary, key) == response[key]


def test_report(client, core):
    response = {"status": "created", "elapsed": {}, "errors": [], "warnings": []}

    uuid = "18f61a79-208f-4201-91cc-5adff8d600f0"
    client.bind(uuid)
    core.add_response(f"/ingests/{uuid}/report", response)

    report = client.report
    assert_core_response(core, client, "GET", f"/ingests/{uuid}/report", response)

    assert isinstance(report, T.Report)
    assert isinstance(report.status, T.IngestStatus)


def test_tree(client, core):
    response = (
        '{"id":"18f61a79-208f-4201-91cc-5adff8d600f0",'
        '"ingest_id":"18f61a79-208f-4201-91cc-5adff8d600f0",'
        '"files_cnt":5,'
        '"bytes_sum":6,'
        '"parent_id":"18f61a79-208f-4201-91cc-5adff8d600f0",'
        '"path":"str",'
        '"level":2,'
        '"src_context":{"label":"prj"},'
        '"dst_context":null,'
        '"dst_path":"str"}'
        "\n"
    )

    uuid = "18f61a79-208f-4201-91cc-5adff8d600f0"
    client.bind(uuid)
    core.add_response(f"/ingests/{uuid}/tree", response)

    tree = list(client.tree)
    assert_core_response(
        core, client, "GET", f"/ingests/{uuid}/tree", response, convert=True
    )

    assert len(tree) == 1
    tree = tree[0]
    assert isinstance(tree, T.Container)


def test_audit_logs(client, core):
    response = "\n\n\nline1\n\n\n\n\n\nline2\nline3\n\nline4\n\n\n"

    uuid = "18f61a79-208f-4201-91cc-5adff8d600f0"
    client.bind(uuid)
    core.add_response(f"/ingests/{uuid}/audit", response)
    logs = list(client.audit_logs)
    resp = client.call_api("GET", f"/ingests/{uuid}/audit", stream=True)
    assert resp.status_code == 200
    assert logs == ["line1\n", "line2\n", "line3\n", "line4\n"]


def test_deid_logs(client, core):
    response = "\n\n\nline1\n\n\n\n\n\nline2\nline3\n\nline4\n\n\n"

    uuid = "18f61a79-208f-4201-91cc-5adff8d600f0"
    client.bind(uuid)
    core.add_response(f"/ingests/{uuid}/deid", response)

    logs = list(client.deid_logs)
    resp = client.call_api("GET", f"/ingests/{uuid}/deid", stream=True)
    assert resp.status_code == 200
    assert logs == ["line1\n", "line2\n", "line3\n", "line4\n"]


def test_subjects(client, core):
    response = "\n\n\nline1\n\n\n\n\n\nline2\nline3\n\nline4\n\n\n"

    uuid = "18f61a79-208f-4201-91cc-5adff8d600f0"
    client.bind(uuid)
    core.add_response(f"/ingests/{uuid}/subjects", response)

    logs = list(client.subjects)
    resp = client.call_api("GET", f"/ingests/{uuid}/subjects", stream=True)
    assert resp.status_code == 200

    assert logs == ["line1\n", "line2\n", "line3\n", "line4\n"]


def test_delete_ingest(client, core):
    response = {"foo": "bar"}

    uuid = "18f61a79-208f-4201-91cc-5adff8d600f0"
    core.add_response(f"/ingests/{uuid}/delete", response, method="POST")

    client.delete_ingest(uuid)
    assert_core_response(core, client, "POST", f"/ingests/{uuid}/delete", response)


def test_connect_timeout_env(mock_request, mocker, core):
    os.environ["FLYWHEEL_CLI_CONNECT_TIMEOUT"] = "40"
    # reimport beacuse of module level timeout vars
    importlib.reload(util)
    # need to mock get_api_key again after reload
    mocker.patch("flywheel_cli.util.get_api_key", return_value=_API_KEY)

    client = api_client.APIClient(core.url)

    response = "sample_response"
    mock_request.return_value = MockResponse(response, 200)

    resp = client.call_api("GET", "/foo")
    assert resp == response
    mock_request.assert_called_once_with("GET", core.url + "/foo", timeout=(40, 60))

    del os.environ["FLYWHEEL_CLI_CONNECT_TIMEOUT"]


def test_connect_timeout_env_raise(mocker):
    os.environ["FLYWHEEL_CLI_CONNECT_TIMEOUT"] = "abc"
    with pytest.raises(ValueError):
        importlib.reload(util)

    del os.environ["FLYWHEEL_CLI_CONNECT_TIMEOUT"]

    os.environ["FLYWHEEL_CLI_READ_TIMEOUT"] = "abc"
    with pytest.raises(ValueError):
        importlib.reload(util)

    del os.environ["FLYWHEEL_CLI_READ_TIMEOUT"]


def test_read_timeout_env(mock_request, mocker, core):
    os.environ["FLYWHEEL_CLI_READ_TIMEOUT"] = "32"
    # reimport beacuse of module level timeout vars
    importlib.reload(util)
    # need to mock get_api_key again after reload
    mocker.patch("flywheel_cli.util.get_api_key", return_value=_API_KEY)

    client = api_client.APIClient(core.url)

    response = "sample_response"
    mock_request.return_value = MockResponse(response, 200)

    resp = client.call_api("GET", "/foo")
    assert resp == response
    mock_request.assert_called_once_with("GET", f"{core.url}/foo", timeout=(30, 32))

    del os.environ["FLYWHEEL_CLI_READ_TIMEOUT"]


@pytest.fixture(scope="function")
def mock_request(mocker, mock_get_api_key):
    return mocker.patch("requests.Session.request")


@pytest.fixture(scope="function")
def mock_get_api_key(mocker):
    return mocker.patch("flywheel_cli.util.get_api_key", return_value=_API_KEY)


@pytest.fixture(scope="function")
def client(mocker, mock_get_api_key, core):
    return api_client.APIClient(url=core.url)


@pytest.fixture
def core(http_testserver):
    return http_testserver


class MockResponse(Response):
    def __init__(self, json_data, status_code):
        self.json_data = json_data
        self.status_code = status_code
        self.reason = ""
        self.encoding = "utf-8"
        self._content = json.dumps(json_data).encode("utf-8")

    def raise_for_status(self):
        if self.status_code != 200:
            raise Exception(f"Response code is not 200 ({self.status_code})")

    def json(self):
        return self.json_data

    def iter_lines(self):
        lines = self.json_data.split("\n")
        for line in lines:
            line = line.strip().encode("utf-8")
            yield line


def assert_ingest(ingest: T.IngestOutAPI, ingest_json):
    assert isinstance(ingest, T.IngestOutAPI)
    assert isinstance(ingest.id, UUID)
    assert isinstance(ingest.created, datetime.datetime)
    assert ingest.status == ingest_json["status"]
    assert ingest.config.src_fs == ingest_json["config"]["src_fs"]  # type: ignore
    assert ingest.fw_host == ingest_json["fw_host"]
    assert ingest.fw_user == ingest_json["fw_user"]
    assert ingest.created.strftime("%Y-%m-%d %H:%M:%S") == ingest_json["created"]
    # TODO assert strategy_config


def assert_mock_call(mocked_function, method, path, core, **kwargs):
    calls = call(method, core.url + path, timeout=(30, 60), **kwargs)
    print(calls)
    assert mocked_function.mock_calls == [calls]


def assert_core_response(core, client, method, path, response, convert=False):
    """Check that the response from the core server is the same as the response from the client."""
    if isinstance(response, str) and convert:
        response = json.loads(response)  # force conversion of response to dict
    core.add_response(path, response, method)
    resp = client.call_api(method, path, json=response, stream=True)
    assert isinstance(resp, Response)
    assert len(resp.json()) == len(response)
    if isinstance(resp.json(), list):
        TestCase().assertListEqual(resp.json(), json.loads(json.dumps(response)))
    elif isinstance(resp.json(), dict):
        TestCase().assertDictEqual(resp.json(), json.loads(json.dumps(response)))
    else:
        assert resp.json() == json.loads(json.dumps(response))
    core.reset()
