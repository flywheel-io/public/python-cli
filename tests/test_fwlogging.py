import logging
from typing import Any

import pytest

from flywheel_cli.fwlogging import DeprecatedAdapter, deprecated

# # Configure logging
# log = logging.getLogger("")
# log.setLevel(logging.ERROR)
# console_handler = logging.StreamHandler()
# console_handler.setLevel(logging.ERROR)
# log.addHandler(console_handler)

PARAMS_DEPRECATED: Any = [
    None,
    ((), {}),
    ((), {"Please use function Bar"}, {}),
    ((), {"message": "Please use function Bar"}),
    ((), {"name": "Foo"}),
    ((), {"version": "1.2.3"}),
]


# Function test
@pytest.fixture(scope="module", params=PARAMS_DEPRECATED)
def deprecated_decorator_function(request, caplog):
    if request.param is None:
        with caplog.at_level(logging.DEBUG):

            @deprecated
            def Foo1():
                pass

    else:
        args, kwargs = request.param
        with caplog.at_level(logging.DEBUG):

            @deprecated(*args, **kwargs)
            def Foo1():
                pass

    return Foo1


# Object test
@pytest.fixture(scope="module", params=PARAMS_DEPRECATED)
def deprecated_decorator_object(request, caplog):
    if request.param is None:
        with caplog.at_level(logging.DEBUG):

            @deprecated
            def Foo2(object):
                pass

    else:
        args, kwargs = request.param
        with caplog.at_level(logging.DEBUG):

            @deprecated(*args, **kwargs)
            def Foo2(object2):
                pass

    return Foo2


# Class method test
@pytest.fixture(scope="module", params=PARAMS_DEPRECATED)
def deprecated_decorator_method(request, caplog):
    if request.param is None:
        with caplog.at_level(logging.DEBUG):

            class Foo3:
                @deprecated
                def foo3(self):
                    pass

    else:
        args, kwargs = request.param
        with caplog.at_level(logging.DEBUG):

            class Foo3:
                @deprecated(*args, **kwargs)
                def foo3(self):
                    pass

    return Foo3


# @staticmethod test
@pytest.fixture(scope="module", params=PARAMS_DEPRECATED)
def deprecated_decorator_static_method(request, caplog):
    if request.param is None:
        with caplog.at_level(logging.DEBUG):

            class Foo4:
                @staticmethod
                @deprecated
                def foo4():
                    pass

    else:
        args, kwargs = request.param
        with caplog.at_level(logging.DEBUG):

            class Foo4:
                @staticmethod
                @deprecated(*args, **kwargs)
                def foo4():
                    pass

    return Foo4.foo4


# @classmethod test
@pytest.fixture(scope="module", params=PARAMS_DEPRECATED)
def deprecated_decorator_class_method(request, caplog):
    if request.param is None:
        with caplog.at_level(logging.DEBUG):

            class Foo5:
                @classmethod
                @deprecated
                def foo5(cls):
                    pass

    else:
        args, kwargs = request.param
        with caplog.at_level(logging.DEBUG):

            class Foo5:
                @classmethod
                @deprecated(*args, **kwargs)
                def foo5(cls):
                    pass

    return Foo5


# Test whether an error is passed when non-string name is provided
def test_deprecated_name_string():
    name = 5
    try:
        DeprecatedAdapter(name=name)
        assert False, "TypeError not raised"
    except TypeError:
        pass


# Test whether an error is passed when non-string version is provided
def test_deprecated_version_string():
    version = 16.2
    try:
        DeprecatedAdapter(version=version)
        assert False, "TypeError not raised"
    except TypeError:
        pass


# Test whether an error is passed when non-string message is provided
def test_deprecated_message_string():
    message = 5
    try:
        DeprecatedAdapter(message=message)
        assert False, "TypeError not raised"
    except TypeError:
        pass


# Test whether name is correctly printed in warning
def test_deprecated_name(caplog):
    name = "Bar"

    @deprecated(name=name)
    def Foo():
        pass

    Foo()
    warn = [rec.message for rec in caplog.records][0]
    assert name in str(warn)


# Test whether version is correctly printed in warning
def test_deprecated_version(caplog):
    version = "16.2"

    @deprecated(version=version)
    def Foo():
        pass

    Foo()
    warn = [rec.message for rec in caplog.records][0]
    assert version in str(warn)


# Test whether message is correctly printed in warning
def test_deprecated_message(caplog):
    message = "Please use function Bar instead."

    @deprecated(message=message)
    def Foo():
        pass

    Foo()
    warn = [rec.message for rec in caplog.records][0]
    assert message in str(warn)
