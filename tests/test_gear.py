import pytest

from flywheel_cli import main


def test_gear_command_w_admin(mocker, capsys):
    mocker.patch("flywheel_cli.util.load_auth_config", return_value={"root": True})
    with pytest.raises(SystemExit):
        main.main(("admin", "gears"))
    captured = capsys.readouterr()

    assert "Enable gear" in captured.out
    assert "Disable gear" in captured.out


def test_gear_command_wo_admin(mocker, capsys):
    mocker.patch("flywheel_cli.util.load_auth_config", return_value={"root": False})
    with pytest.raises(SystemExit):
        main.main(("admin", "gears"))
    captured = capsys.readouterr()

    assert captured.err.startswith("usage: fw")


def test_gear_command_wo_admin_bad_place(mocker, capsys):
    mocker.patch("flywheel_cli.util.load_auth_config", return_value={"root": True})
    with pytest.raises(SystemExit):
        main.main(("gears"))
    captured = capsys.readouterr()

    assert captured.err.startswith("usage: fw")
