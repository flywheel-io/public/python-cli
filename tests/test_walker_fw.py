"""
Tests of FW Walker class.

Prerequisites:
    - It should implement the abstract Walker class.

Expected behavior:
    - It should iterate through a given project's files in a FW instance via
      the list_files method by calling its download/targets endpoint.
    - It should download the given files from FW instance via the open method where
      the path is the file's container_id/filename to be able to use FW's endpoint.
"""

import json
import os
import tempfile

import flask
import pytest

from flywheel_cli.walker.fw_walker import FWFileInfo, FWMetaFileInfo, FWWalker

pytest_plugins = "fw_http_testserver"


def test_listing_files_via_iterating_trough_download_targets_lines(http_testserver):
    """
    Positive test of listing a project's files by iterating through the JSON lines
    of download/targets endpoint's response.
    """
    files = [
        {
            "dst_path": "test-path1",
            "size": "test-size1",
            "modified": "2012-01-19 17:21:00 GMT",
            "container_id": "test-container-id1",
            "filename": "test-filename1",
            "filetype": "test-filetype1",
            "download_type": "test-download-type1",
        },
        {
            "dst_path": "test-path2",
            "modified": "2012-01-19 17:21:00 GMT",
            "container_id": "test-container-id2",
            "filename": "test-filename2",
            "filetype": "test-filetype2",
            "download_type": "metadata_sidecar",
            "size": "indifferent",
            "metadata": {},
        },
    ]

    def line_by_line():
        def iter_on_files():
            for f in files:
                yield json.dumps(f) + "\n"

        return flask.Response(iter_on_files(), mimetype="application/json")

    http_testserver.add_callback(
        "/api/download/targets", line_by_line, methods=("POST",)
    )
    http_testserver.add_response("/api/auth/status", body="{}")
    http_testserver.add_response("/api/users/self", body="{}")
    http_testserver.add_response("/api/config", body="{}")
    http_testserver.add_response("/api/version", body="{}")
    http_testserver.add_response(
        "/api/lookup",
        body={"container_type": "project", "_id": "test-project-id1"},
        method="POST",
    )
    chunk_counter = 0

    for f in FWWalker(
        "test-project/test-group", f"{http_testserver.addr}:__force_insecure"
    ).list_files():
        if files[chunk_counter]["download_type"] == "metadata_sidecar":
            assert isinstance(f, FWMetaFileInfo)
            assert f.size == len(
                json.dumps(files[chunk_counter]["metadata"]).encode("utf-8")
            )
            assert f.content == files[chunk_counter]["metadata"]
        else:
            assert isinstance(f, FWFileInfo)
            assert f.size == files[chunk_counter]["size"]
            assert f.filename == files[chunk_counter]["filename"]
            assert f.container_id == files[chunk_counter]["container_id"]

        assert f.name == files[chunk_counter]["dst_path"]

        chunk_counter += 1

    assert chunk_counter == len(files)
    for req in http_testserver.requests:
        if req["url"] == "/api/download/targets":
            assert (
                req["body"].decode("utf-8")
                == '{"nodes": [{"level": "project", "_id": "test-project-id1"}]}'
            )

            assert req["headers"]["Authorization"] == "scitran-user __force_insecure"


def test_open_file(http_testserver):
    """Positive test of downloading the given file from a FW instance."""
    http_testserver.add_response("/api/auth/status", body="{}")
    http_testserver.add_response("/api/users/self", body="{}")
    http_testserver.add_response(
        "/api/containers/test-container-id/files/test-file-name", body="test-content"
    )

    f = FWWalker(
        "test-project/test-group", f"{http_testserver.addr}:__force_insecure"
    ).open("test-container-id/test-file-name")

    assert hasattr(f, "read")  # file-like object
    assert f.read().decode("utf-8") == "test-content"


@pytest.fixture
def custom_tempdir():
    tempfile.tempdir = "/tmp/test-fw-walker"
    if not os.path.isdir(tempfile.tempdir):
        os.mkdir(tempfile.tempdir)

    yield tempfile.tempdir

    os.removedirs(tempfile.tempdir)
    tempfile.tempdir = None


def test_close_file(http_testserver, custom_tempdir):
    """Positive test of closing the temporary file attached to the walker."""
    http_testserver.add_response("/api/auth/status", body="{}")
    http_testserver.add_response("/api/users/self", body="{}")
    w = FWWalker("test-project/test-group", f"{http_testserver.addr}:__force_insecure")
    assert len(os.listdir(tempfile.tempdir)) == 1

    w.close()
    assert len(os.listdir(tempfile.tempdir)) == 0
