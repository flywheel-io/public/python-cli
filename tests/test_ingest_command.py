from unittest import mock

from flywheel_cli.commands.ingest import Command, IngestCommand

from .conftest import AttrDict


def test_add_argument_groups():
    argparser_mock = mock.Mock()
    command = Command(argparser_mock, "name")

    argparser_mock.reset_mock()
    groups = command.add_argument_groups()
    assert len(groups) == 1
    assert argparser_mock.mock_calls == [
        mock.call.add_parser().add_argument_group(title="General"),
    ]


def test_add_arguments():
    class DummyCommand(Command):
        arg_groups = {}
        arg_table = [
            {
                "name": "z_name",
                "positional": True,
                "help": "z_name",
            },
            {"name": "c_name", "help": "c_name", "default": "123"},
            {
                "name": "a_name",
                "positional": True,
                "help": "a_name",
            },
            {
                "name": "d_name",
                "help": "d_name",
            },
            {
                "name": "b_name",
                "help": "b_name",
            },
            {
                "name": "a_name",
                "positional": True,
                "help": "a_name_2",
            },
            {
                "name": "b_name",
                "help": "b_name_2",
            },
        ]

    argparser_mock = mock.Mock()
    command = DummyCommand(argparser_mock, "name")

    argparser_mock.reset_mock()
    command.add_arguments()
    assert argparser_mock.mock_calls == [
        mock.call.add_parser().add_argument("z_name", default=None, help="z_name"),
        mock.call.add_parser().add_argument("a_name", default=None, help="a_name"),
        mock.call.add_parser().add_argument(
            "--b-name", default=None, dest="b_name", help="b_name"
        ),
        mock.call.add_parser().add_argument(
            "--c-name", default=None, dest="c_name", help="c_name (default: 123)"
        ),
        mock.call.add_parser().add_argument(
            "--d-name", default=None, dest="d_name", help="d_name"
        ),
    ]


def test_load_subject_csv(db, tmpdir):
    class DummyCommand(IngestCommand):
        def __init__(self, path, *args, **kwargs):
            self.config = AttrDict({"ingest": {"load_subjects": path}})

    db.create_ingest(
        config={
            "subject_config": {
                "code_serial": 1,
                "code_format": "code-{SubjectCode}",
                "map_keys": [],
            }
        }
    )

    fname = str(tmpdir.join("subjects.csv"))
    with open(fname, "w", encoding="utf-8") as f:
        f.write("code-{SubjectCode}\n")
        f.write("code-1,code_a\n")
        f.write("code-2,code_b\n")
    command = DummyCommand(fname)
    command.load_subjects(db.client)
    subjects = list(db.client.subjects)
    assert subjects == ["code-{SubjectCode}\n", "code-1,code_a\n", "code-2,code_b\n"]
