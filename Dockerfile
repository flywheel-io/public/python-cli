FROM flywheel/python:3.12-alpine-build AS base
SHELL ["/bin/bash", "-euxo", "pipefail", "-c"]
WORKDIR /src
COPY requirements.txt ./
RUN uv pip install -rrequirements.txt

FROM base AS dev
ENV GOPATH=/usr/local
RUN apk --no-cache add go make; \
    rm -rf /var/lib/apt/lists/*; \
    go install github.com/go-bindata/go-bindata/...@latest; \
    go install github.com/mitchellh/gox@v1.0.1; \
    uv pip install gsutil poetry pyopenssl==24.2.1
COPY requirements-dev.txt ./
RUN uv pip install -rrequirements-dev.txt
COPY . .
RUN uv pip install --no-deps -e.

FROM base
COPY . .
RUN uv pip install --no-deps -e.; \
    /cleanup_build_deps.sh
USER flywheel
