SHELL     := bash
SHELLOPT  := -Eeuo pipefail
PLATFORM  ?= $(shell uname -sm | tr "[:upper:]" "[:lower:]" | tr " " "-")
CACHE_DIR := .cache

PYTHON_VERSION   := 3.12.8
PYTHON_MINOR     := $(shell PYTHON_VERSION=$(PYTHON_VERSION); echo $${PYTHON_VERSION%.*})
PYTHON_BUILD_DIR := build/python-$(PLATFORM)
ifeq ($(PLATFORM),windows-x86_64)
PYTHON_BUILD_LIB_DIR := $(PYTHON_BUILD_DIR)/python/install/Lib
else
PYTHON_BUILD_LIB_DIR := $(PYTHON_BUILD_DIR)/python/install/lib
endif
PYTHON_ARCHIVE  := dist/python-$(PYTHON_VERSION)-$(PLATFORM).tar.gz
PYTHON_RELEASES := $(CACHE_DIR)/python-build-standalone-releases.json
PYTHON_DOWNLOAD := $(CACHE_DIR)/python-$(PYTHON_VERSION)_$(PLATFORM).tar.zst

ifeq ($(PLATFORM),linux-x86_64)
PLATFORM_PYTHON := cpython-$(PYTHON_VERSION).*x86_64-unknown-linux-gnu-pgo%2Blto
PLATFORM_WHEEL  := manylinux_2_17_x86_64  # glibc sync w/ py-standalone and LTS
GO_OSARCH       := linux/amd64
else ifeq ($(PLATFORM),darwin-x86_64)
PLATFORM_PYTHON := cpython-$(PYTHON_VERSION).*x86_64-apple-darwin-pgo%2Blto
PLATFORM_WHEEL  := macosx_12_0_x86_64
GO_OSARCH       := darwin/amd64
else ifeq ($(PLATFORM),windows-x86_64)
PLATFORM_PYTHON := cpython-$(PYTHON_VERSION).*x86_64-pc-windows-msvc-pgo
PLATFORM_WHEEL  := win_amd64
GO_OSARCH       := windows/amd64
endif

COMMIT := $(or $(CI_COMMIT_SHORT_SHA),$(shell git rev-parse --short=8 HEAD))
ifdef CI_COMMIT_TAG
CLI_FULLVER := $(CI_COMMIT_TAG)
CLI_VERSION := $(CI_COMMIT_TAG)
else
VERSION        := $(shell grep "^version" pyproject.toml | sed -E 's/.*"(.*)"/\1/')
BRANCH         := $(or $(CI_COMMIT_REF_NAME),$(shell git rev-parse --abbrev-ref HEAD))
DEFAULT_BRANCH := $(or $(CI_DEFAULT_BRANCH),master)
CLI_FULLVER    := $(VERSION)+$(BRANCH).$(COMMIT)
CLI_VERSION    := $(subst $(DEFAULT_BRANCH),latest,$(BRANCH))
endif

CLI_BUILD_DIR    := build/cli-$(PLATFORM)
ifeq ($(PLATFORM),windows-x86_64)
CLI_BUILD_SUBDIR := $(CLI_BUILD_DIR)/Lib/site-packages
else
CLI_BUILD_SUBDIR := $(CLI_BUILD_DIR)/lib/python$(PYTHON_MINOR)/site-packages
endif
CLI_PKGDATA_DIR  := build/cli-$(PLATFORM)-pkgdata
CLI_WHEEL_VER    := $(shell echo $(CLI_FULLVER) | tr '[:upper:]' '[:lower:]' | tr - .)
CLI_WHEEL        := dist/flywheel_cli-$(CLI_WHEEL_VER)-py3-none-any.whl
CLI_ARCHIVE      := dist/flywheel-cli-$(CLI_VERSION)-$(PLATFORM).tar.gz
CLI_BIN_DIR      := $(CURDIR)/build/cli-bin-$(PLATFORM)
CLI_BIN          := $(CLI_BIN_DIR)/$(subst /,_,$(GO_OSARCH))/fw
CLI_BIN_ARCHIVE  := $(CURDIR)/dist/fw-$(subst /,_,$(GO_OSARCH))-$(CLI_VERSION).zip
PIP_INSTALL_ARGS := --no-deps --no-compile --upgrade --target $(CLI_BUILD_SUBDIR)

GO_SRC           := $(CURDIR)/go
GO_PKGDATA       := $(GO_SRC)/python/pkgdata/pkgdata_$(subst /,_,$(GO_OSARCH)).go
BUILD_TIME       := $(shell date --utc --iso-8601=seconds)
COMMON_LDFLAGS   := -X "main.Version=$(CLI_FULLVER)" -X "main.BuildHash=$(COMMIT)" -X "main.BuildDate=$(BUILD_TIME)" -s -w

# github started playing dirty against bots - adding auth if available
ifdef GITHUB_TOKEN
GITHUB_HEADERS := 'authorization:bearer $(GITHUB_TOKEN)'
endif


all: linux-x86_64 darwin-x86_64 windows-x86_64  ## Create all-platform dist/ folder
	make dist

linux-x86_64 darwin-x86_64 windows-x86_64:
	PLATFORM=$@ make build

build: $(CLI_BIN_ARCHIVE)  ## Build python and cli for PLATFORM

dist: $(PLATFORM)  ## Create dist/ folder for PLATFORM
	cd dist; \
	SHA_CMD=`which shasum &>/dev/null && echo "shasum -a256" || echo sha256sum`; \
	for FILE in `find . -type f -name '*.zip' | sed -E 's|^./||' | sort`; do \
		$$SHA_CMD $$FILE >$$FILE.sha256; \
	done

clean:  ## Delete all files generated via make
	rm -rf build dist install go/python $(CACHE_DIR)

help:  ## Show this help and exit
	@printf "Usage: make TARGET\n\n"
	@grep "##" Makefile | grep -v Makefile | sed -E "s/(.*):.*## (.*)/\1\t\2/;s/PLATFORM/$(PLATFORM)/"


$(CLI_BIN_ARCHIVE): $(CLI_BIN)
	cd $(CLI_BIN_DIR); zip -r $(CLI_BIN_ARCHIVE) ./*

$(CLI_BIN): $(GO_PKGDATA)
	cd $(GO_SRC); \
	go mod vendor; \
	CGO_ENABLED=0 GOFLAGS="-mod=vendor" gox -ldflags '$(COMMON_LDFLAGS)' -osarch=$(GO_OSARCH) -output $(CLI_BIN)


$(GO_PKGDATA): $(CLI_ARCHIVE) $(PYTHON_ARCHIVE)
	mkdir -p $(CLI_PKGDATA_DIR)
	cp $(CLI_ARCHIVE) $(CLI_PKGDATA_DIR)
	cp $(PYTHON_ARCHIVE) $(CLI_PKGDATA_DIR)
	jq -n \
		--arg python_version $(PYTHON_VERSION) \
		--arg py_ver $(PYTHON_MINOR) \
		--arg cli_version $(CLI_VERSION) \
		--arg platform $(PLATFORM) \
		--arg build_time $(BUILD_TIME) \
		'$$ARGS.named' >$(CLI_PKGDATA_DIR)/version.json
	go-bindata -nocompress -pkg="pkgdata" -prefix $(CLI_PKGDATA_DIR) -o $(GO_PKGDATA) $(CLI_PKGDATA_DIR)


$(CLI_ARCHIVE): $(CLI_WHEEL)
	mkdir -p $(CLI_BUILD_SUBDIR) dist
	poetry run pip install --upgrade pip
	poetry run pip install $(PIP_INSTALL_ARGS) $(CLI_WHEEL)
	poetry run pip install $(PIP_INSTALL_ARGS) \
		--platform=$(PLATFORM_WHEEL) \
		--python-version=$(PYTHON_VERSION) \
		--only-binary=:all: \
		--requirement=requirements.txt
	cd $(CLI_BUILD_SUBDIR); \
	find . -type d \( -name test -o -name tests -o -name __pycache__ \) | xargs rm -rf; \
	python$(PYTHON_MINOR) -m compileall -bqj0 . 2>&1 | grep -v WmDefault.py || :; \
	find . -type f -name '*.py' | xargs rm -f
	echo $(PYTHON_VERSION) >$(CLI_BUILD_DIR)/PYTHON_VERSION
	tar czf $(CLI_ARCHIVE) -C $(CLI_BUILD_DIR) .


$(PYTHON_ARCHIVE):
	mkdir -p $(PYTHON_BUILD_DIR) dist $(CACHE_DIR)
	test -s $(PYTHON_RELEASES) || xh -IFo$(PYTHON_RELEASES) \
		https://api.github.com/repos/astral-sh/python-build-standalone/releases \
		$(GITHUB_HEADERS)
	jq -r ".[].assets[].browser_download_url" $(PYTHON_RELEASES) \
		| grep $(PLATFORM_PYTHON) | head -n1 >$(PYTHON_BUILD_DIR)/python-build-standalone
	test -s $(PYTHON_DOWNLOAD) || xh -IFo$(PYTHON_DOWNLOAD) \
		`cat $(PYTHON_BUILD_DIR)/python-build-standalone` \
		$(GITHUB_HEADERS)
	zstd -cfd $(PYTHON_DOWNLOAD) | tar xC $(PYTHON_BUILD_DIR)
	cd $(PYTHON_BUILD_LIB_DIR); \
	find . -type d \( -name test -o -name tests -o -name __pycache__ \) | xargs rm -rf; \
	python$(PYTHON_MINOR) -m compileall -bqj0 . 2>&1 | grep -v WmDefault.py || :; \
	find . -type f -name '*.py' | xargs rm -f
	rm -rf $(PYTHON_BUILD_DIR)/python/install/share
	tar czf $(PYTHON_ARCHIVE) -C $(PYTHON_BUILD_DIR)/python/install .


$(CLI_WHEEL):
	sed -Ei.bkp "s/^version.*/version = \"$(CLI_FULLVER)\"/" pyproject.toml
	poetry build --format wheel
	mv pyproject.toml.bkp pyproject.toml
